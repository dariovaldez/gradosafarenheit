package facci.erickvaldez.centigradosafarenheit;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    EditText txtValor;
    TextView txtConversion;
    Button btnCalcular;
    RadioButton rb1, rb2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.e("onCreate", "Erick Darío Valdez Cedeño");

        txtValor = (EditText) findViewById(R.id.txtValor);
        txtConversion = (TextView) findViewById(R.id.txtConversion);
        btnCalcular = (Button) findViewById(R.id.btnCalcular);
        rb1 = (RadioButton) findViewById(R.id.Rb1);
        rb2 = (RadioButton) findViewById(R.id.Rb2);

        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calcular(v);
            }
        });
    }


    public void calcular(View v) {
        Double valor = Double.parseDouble(txtValor.getText().toString());
        Double resultado;
        if (rb1.isChecked()) {
            resultado = 1.8 * valor + 32;
            txtConversion.setText(resultado.toString());
        } else if (rb2.isChecked()) {
            resultado = (valor - 32) / 1.8;
            txtConversion.setText(resultado.toString());
        }

    }
}
